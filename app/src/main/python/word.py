import nltk
nltk.download('stopwords')
nltk.download('punkt')
while not nltk.download('averaged_perceptron_tagger'):
    print("Retrying download")
nltk.download('maxent_ne_chunker')
nltk.download('words')

from nltk import pos_tag
from nltk import ne_chunk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
#nltk.download('maxent_ne_chunker')
#nltk.download('words')
def plant_extract(sentence):
    lst=[]
    remove=stopwords.words('english')
    words_token=word_tokenize(sentence)
    stop_remove=[word for word in words_token if word not in remove]
    words_pos=pos_tag(stop_remove)
    named_entity=ne_chunk(words_pos)
    for x in named_entity:
        if x[1]=='NN':
            lst.append(x[0])
    return lst
