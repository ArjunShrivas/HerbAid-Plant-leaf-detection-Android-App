
package com.example.herbheal.Model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Result  implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("p_name")
    @Expose
    private String pName;
    @SerializedName("p_name_hi")
    @Expose
    private String pNameHi;
    @SerializedName("p_benifits")
    @Expose
    private String pBenifits;
    @SerializedName("p_benifits_hi")
    @Expose
    private String pBenifitsHi;

    public String getPimages() {
        return pimages;
    }

    public void setPimages(String pimages) {
        this.pimages = pimages;
    }

    @SerializedName("p_images")
    @Expose
    private String pimages;

    protected Result(Parcel in) {
        id = in.readString();
        pName = in.readString();
        pNameHi = in.readString();
        pBenifits = in.readString();
        pBenifitsHi = in.readString();
        pimages= in.readString();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpNameHi() {
        return pNameHi;
    }

    public void setpNameHi(String pNameHi) {
        this.pNameHi = pNameHi;
    }

    public String getpBenifits() {
        return pBenifits;
    }

    public void setpBenifits(String pBenifits) {
        this.pBenifits = pBenifits;
    }

    public String getpBenifitsHi() {
        return pBenifitsHi;
    }

    public void setpBenifitsHi(String pBenifitsHi) {
        this.pBenifitsHi = pBenifitsHi;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(pName);
        dest.writeString(pNameHi);
        dest.writeString(pBenifits);
        dest.writeString(pBenifitsHi);
        dest.writeString(pimages);

    }
}
