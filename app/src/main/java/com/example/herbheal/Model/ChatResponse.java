package com.example.herbheal.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatResponse {
    public String getChatbotrply() {
        return chatbotrply;
    }

    public void setChatbotrply(String chatbotrply) {
        this.chatbotrply = chatbotrply;
    }

    @SerializedName("chatbotrply")
    @Expose
    private String chatbotrply;
}
