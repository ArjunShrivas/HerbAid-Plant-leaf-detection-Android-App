package com.example.herbheal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.herbheal.Model.ChatResponse;
import com.example.herbheal.Model.PlantDetails;
import com.example.herbheal.Model.Result;
import com.example.herbheal.service.ApiClient;
import com.example.herbheal.service.ChatbotApiClient;
import com.example.herbheal.service.RecyclerInterface;
import com.example.herbheal.service.SessionManager;
import com.example.herbheal.service.VoiceRecognizerDialogFragment;
import com.google.gson.Gson;
import com.thekhaeng.pushdownanim.PushDownAnim;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {
LinearLayout ln_backpress,ln_empty_box;
List<Result> Plant_details;
String p_name,p_name_hi,p_benifits,p_benifits_hi;
TextView txt_discription,txt_about,txt_cem_name,txt_name;
ImageView img_info;
SessionManager sessionManager;
ProgressBar processbasr;
ConstraintLayout cons_main;
RecyclerInterface apiInterface,apiInterface_chatbot;
String image_path="https://healherb.000webhostapp.com/heal_images/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ln_backpress=findViewById(R.id.ln_backpress);
        txt_discription=findViewById(R.id.txt_discription);
        txt_about=findViewById(R.id.txt_about);
        txt_cem_name=findViewById(R.id.txt_cem_name);
        txt_name=findViewById(R.id.txt_name);
        img_info=findViewById(R.id.img);
        processbasr=findViewById(R.id.processbasr);
        cons_main=findViewById(R.id.cons_main);
        ln_empty_box=findViewById(R.id.ln_empty_box);

        apiInterface= ApiClient.getClient().create(RecyclerInterface.class);
        apiInterface_chatbot= ChatbotApiClient.getClient().create(RecyclerInterface.class);
        sessionManager = new SessionManager();
         Intent i = getIntent();
         Plant_details = i.getExtras().getParcelableArrayList("details");
         String spokenText =i.getStringExtra("spokenText");
         if(!Plant_details.isEmpty()){
             initviwes(Plant_details);
             processbasr.setVisibility(View.GONE);
             cons_main.setVisibility(View.VISIBLE);
         }else {
             GetChatBot(spokenText);

         }
         PushDownAnim.setPushDownAnimTo(ln_backpress).setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
      if(v==ln_backpress){
          onBackPressed();
      }
    }

    //initialize value in views method
    void initviwes(List<Result> plant_details){
        if(plant_details.get(0).getpName()!=null){
            p_name=plant_details.get(0).getpName();
        }else {
            p_name="NA";
        }
        if(plant_details.get(0).getpNameHi()!=null){
            p_name_hi=plant_details.get(0).getpNameHi();
        }else {
            p_name_hi="NA";
        }
        if(plant_details.get(0).getpBenifits()!=null){
            p_benifits=plant_details.get(0).getpBenifits();
        }else {
            p_benifits="NA";
        }
        if(plant_details.get(0).getpBenifitsHi()!=null){
            p_benifits_hi=plant_details.get(0).getpBenifitsHi();
        }else {
            p_benifits_hi="NA";
        }

        if(plant_details.get(0).getPimages()!=null){
            Glide.with(this).load(image_path+plant_details.get(0).getPimages()).placeholder(R.drawable.progress_animation).into(img_info);
        }

        if(sessionManager.getPreferences(this,"languages") !=null && sessionManager.getPreferences(this,"languages").equalsIgnoreCase("hi")){
            String local_name = p_name_hi.substring(p_name_hi.indexOf("(")+1,p_name_hi.indexOf(")"));
            String[] repl=p_name_hi.split("\\(");
            txt_name.setText(local_name);
            txt_cem_name.setText(repl[0]);
            txt_about.setText(local_name+" "+getResources().getString(R.string.act_deti_benifits));
            txt_discription.setText(p_benifits_hi);

        }else {
            String local_name = p_name.substring(p_name.indexOf("(")+1,p_name.indexOf(")"));
            txt_name.setText(local_name);
            String[] repl=p_name.split("\\(");
            txt_cem_name.setText(repl[0]);
            txt_about.setText(getResources().getString(R.string.act_deti_benifits)+" "+local_name);
            txt_discription.setText(p_benifits);
        }

        processbasr.setVisibility(View.GONE);
        cons_main.setVisibility(View.VISIBLE);
        ln_empty_box.setVisibility(View.GONE);

    }
    private void GetChatBot(final  String p_name) {

        Call<ChatResponse> call = apiInterface_chatbot.getChatbot(p_name);
        call.enqueue(new Callback<ChatResponse>() {
            @Override
            public void onResponse(Call<ChatResponse> call, retrofit2.Response<ChatResponse> response) {
                Log.e("TAG", "chatbot list : " + new Gson().toJson(response.body()));

                if(response.isSuccessful()){

                    if(response.body().getChatbotrply().length()>0){
                        GetDetails(response.body().getChatbotrply());
                    }else {
                        processbasr.setVisibility(View.GONE);
                        cons_main.setVisibility(View.GONE);
                        ln_empty_box.setVisibility(View.VISIBLE);
                    }
                }else {
                    processbasr.setVisibility(View.GONE);
                    cons_main.setVisibility(View.GONE);
                    ln_empty_box.setVisibility(View.VISIBLE);
                 }

            }

            @Override
            public void onFailure(Call<ChatResponse> call, Throwable t) {
                processbasr.setVisibility(View.GONE);
                cons_main.setVisibility(View.GONE);
                ln_empty_box.setVisibility(View.VISIBLE);

                Log.e("TAG", "onFailure: " + t.toString());
                // Log error here since request failed
                Toast.makeText(DetailsActivity.this,"Check Internet Speed",Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void GetDetails(final  String p_name) {
        Call<PlantDetails> call = apiInterface.getDetails("get_plant_details.php?p_name="+p_name);
        call.enqueue(new Callback<PlantDetails>() {
            @Override
            public void onResponse(Call<PlantDetails> call, retrofit2.Response<PlantDetails> response) {
                Log.e("TAG", "GetDetails list : " + new Gson().toJson(response.body()));

                if(response.isSuccessful()){
                   initviwes(response.body().getResult());

                }else {
                    processbasr.setVisibility(View.GONE);
                    cons_main.setVisibility(View.GONE);
                    ln_empty_box.setVisibility(View.VISIBLE);
                    Toast.makeText(DetailsActivity.this,"Check Internet Speed",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<PlantDetails> call, Throwable t) {
                processbasr.setVisibility(View.GONE);
                cons_main.setVisibility(View.GONE);
                ln_empty_box.setVisibility(View.VISIBLE);
                // progressBar.setVisibility(View.GONE);
                Log.e("TAG", "onFailure: " + t.toString());
                // Log error here since request failed
                Toast.makeText(DetailsActivity.this,"Check Internet Speed",Toast.LENGTH_SHORT).show();
            }
        });

    }



}