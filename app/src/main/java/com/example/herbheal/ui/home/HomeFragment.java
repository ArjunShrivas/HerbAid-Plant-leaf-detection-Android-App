package com.example.herbheal.ui.home;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.herbheal.Dashboard;
import com.example.herbheal.DetailsActivity;
import com.example.herbheal.R;
import com.example.herbheal.databinding.FragmentHomeBinding;

import com.example.herbheal.service.ApiClient;
import com.example.herbheal.service.DenseNetClassifier;
import com.example.herbheal.service.RecyclerInterface;
import com.example.herbheal.Model.PlantDetails;
import com.example.herbheal.Model.Result;
import com.example.herbheal.service.SessionManager;
import com.google.gson.Gson;
import com.thekhaeng.pushdownanim.PushDownAnim;

import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class HomeFragment extends Fragment implements View.OnClickListener {
    LinearLayout ln_predict,ln_cemra,ln_photo,ln_show_details;
    ImageView img;
    private int mInputSize=180;
    private  String mLabelPath="labels.txt";
    private  String mLabelPath_hi="lables_hi.txt";
    private Bitmap bitmap;
    SessionManager sessionManager;
    TextView txt_predict,confidance;
    private static final int PERMISSION_REQUEST_CODE = 200;
    int  Image_Capture_Code=102;
    private static final int PICK_IMAGE =20;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 101;
    private FragmentHomeBinding binding;
    RecyclerInterface apiInterface;
    static   boolean  click_falg = false;

    /*-xx-x-x-x-x-x-x--x-x*/
    private List<String> labellist;
    private List<String> labellist_hi;
    ArrayList<Result> plant_details;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);

        /* init all View Id */
        View root = binding.getRoot();
        ln_predict = binding.lnPerdict;
        ln_cemra = binding.linearLayout;
        ln_photo = binding.linearLayout2;
        confidance=binding.textView4;
        img = binding.imageView2;
        ln_show_details = binding.lnShowDetails;
        txt_predict = binding.textView2;
        Dashboard.check = 1;
        plant_details=new ArrayList<>();
        sessionManager=new SessionManager();
        apiInterface= ApiClient.getClient().create(RecyclerInterface.class);


        try {
            labellist = loadLabelList(getActivity().getAssets(),mLabelPath);
            labellist_hi= loadLabelList(getActivity().getAssets(),mLabelPath_hi);
        } catch (IOException e) {
            e.printStackTrace();
        }



        // Setonlick Event on view
        PushDownAnim.setPushDownAnimTo(ln_predict).setOnClickListener(this);
        PushDownAnim.setPushDownAnimTo(ln_show_details).setOnClickListener(this);
        PushDownAnim.setPushDownAnimTo(ln_cemra).setOnClickListener(this);
        PushDownAnim.setPushDownAnimTo(ln_photo).setOnClickListener(this);

        return root;
    }


 public List<String>  loadLabelList(AssetManager assetManager, String labelPath)throws IOException {
        List<String> labelList = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(assetManager.open(labelPath)));
        String line;
        while ((line=reader.readLine())!=null){
            labelList.add(line);
        }
        reader.close();
        return labelList;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
  //set onclick on views
  @Override
    public void onClick(View v) {
        if(v==ln_predict){
             BitmapDrawable drawable = (BitmapDrawable) img.getDrawable();
             Bitmap bitmap = drawable.getBitmap();


            try {

                // DenseNet Model
                DenseNetClassifier denseNetClassifier =new DenseNetClassifier(bitmap,labellist,getActivity());
                int index=denseNetClassifier.getmax();
                float accu =denseNetClassifier.getAucc();
                 if(sessionManager.getPreferences(getActivity(),"languages") !=null && sessionManager.getPreferences(getActivity(),"languages").equalsIgnoreCase("hi")){

                    txt_predict.setText(labellist_hi.get(index));

                }else {

                    txt_predict.setText(labellist.get(index));
                }

                GetDetails(labellist.get(index));

                txt_predict.setVisibility(View.VISIBLE);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(v==ln_cemra){

            if (checkAndRequestPermissions(getActivity())) {

                chooseImage(getActivity());

            } else {

                chooseImage(getActivity());
            }


        }else if(v==ln_photo){
            if (checkAndRequestPermissions(getActivity())) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , PICK_IMAGE);
            } else {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , PICK_IMAGE);

            }

        }else if(v==ln_show_details){

            Intent i =new Intent(getActivity(), DetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("details", (ArrayList<? extends Parcelable>) plant_details);
            i.putExtras(bundle);
            i.putExtra("spokenText","");
            startActivity(i);
            getActivity().overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

        }

    }


    private void GetDetails(final  String p_name) {
        Call<PlantDetails> call = apiInterface.getDetails("get_plant_details.php?p_name="+p_name);
        call.enqueue(new Callback<PlantDetails>() {
            @Override
            public void onResponse(Call<PlantDetails> call, retrofit2.Response<PlantDetails> response) {
                Log.e("TAG", "GetDetails list : " + new Gson().toJson(response.body()));

                if(response.isSuccessful()){
                    plant_details= (ArrayList<Result>) response.body().getResult();

                     ln_show_details.setVisibility(View.VISIBLE);
                }else {
                    Toast.makeText(getActivity(),"something worng ",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PlantDetails> call, Throwable t) {
              Log.e("TAG", "onFailure: " + t.toString());
              Toast.makeText(getActivity(), "Check Internet Speed", Toast.LENGTH_SHORT).show();

            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( requestCode == Image_Capture_Code) {
            try {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                img.setImageBitmap(thumbnail);
                confidance.setVisibility(View.INVISIBLE);
                }catch (NullPointerException e){}


        }else  if (requestCode == PICK_IMAGE) {
            try {
                Uri selectedImage = data.getData();

                Toast.makeText(getActivity(), " Granted", Toast.LENGTH_SHORT).show();

                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);

                img.setImageBitmap(bitmap);
                confidance.setVisibility(View.INVISIBLE);
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    // function to let's the user to choose image from camera or gallery
    private void chooseImage(Context context){
        final CharSequence[] optionsMenu = {getResources().getString(R.string.Take), getResources().getString(R.string.choose_grallry), getResources().getString(R.string.Exit) }; // create a menuOption Array
        // create a dialog for showing the optionsMenu
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // set the items in builder
        builder.setItems(optionsMenu, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(sessionManager.getPreferences(getActivity(),"languages") !=null && sessionManager.getPreferences(getActivity(),"languages").equalsIgnoreCase("hi")){
                    // Toast.makeText(this,""+p_name_hi,Toast.LENGTH_SHORT).show();

                    if(optionsMenu[i].equals("तस्वीरें ले")){
                        // Open the camera and get the photo
                        Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(takePicture, Image_Capture_Code);
                    }
                    else if(optionsMenu[i].equals("गैलरी से चुनें फोटो लें")){
                        // choose from  external storage
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto , PICK_IMAGE);
                    }
                    else if (optionsMenu[i].equals("बाहर जाएं")) {
                        dialogInterface.dismiss();
                    }

                }else {

                if(optionsMenu[i].equals("Take Photo")){
                    // Open the camera and get the photo
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, Image_Capture_Code);
                }
                else if(optionsMenu[i].equals("Choose from Gallery")){
                    // choose from  external storage
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , PICK_IMAGE);
                }
                else if (optionsMenu[i].equals("Exit")) {
                    dialogInterface.dismiss();
                }

                }

            }
        });
        builder.show();
    }

    // function to check permission
    public static boolean checkAndRequestPermissions(final Activity context) {
        int WExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (WExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(context, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    // Handled permission Result
    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                } else if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                } else {
                    //chooseImage(getActivity());
                }
                break;
        }
    }

    // find maxmimum probablity
    int getmax(TensorBuffer outputFeature0){
        int index=0;
        float min=0.0f;
        for (int i=0;i<labellist.size();i++){
            if(outputFeature0.getFloatArray()[i]>min){
                index=i;
                min=outputFeature0.getFloatArray()[i];
            }
        }

        return  index;
    }

}