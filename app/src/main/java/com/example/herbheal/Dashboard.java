package com.example.herbheal;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.herbheal.Model.ChatResponse;
import com.example.herbheal.Model.PlantDetails;
import com.example.herbheal.service.ApiClient;
import com.example.herbheal.service.ChatbotApiClient;
import com.example.herbheal.service.RecyclerInterface;
import com.example.herbheal.service.VoiceRecognizerDialogFragment;
import com.example.herbheal.service.VoiceRecognizerInterface;

import com.example.herbheal.ui.home.HomeFragment;
import com.example.herbheal.Model.Result;
import com.example.herbheal.service.SessionManager;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import com.example.herbheal.databinding.ActivityDashboardBinding;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;

public class Dashboard extends AppCompatActivity implements VoiceRecognizerInterface {
    private AppBarConfiguration mAppBarConfiguration;
    private ActivityDashboardBinding binding;
    public  static int check = 1;
    TextView spn_lang;
    SessionManager sessionManager;
    VoiceRecognizerDialogFragment languageDialogFragment;
  public static boolean check_lng=false;
    ArrayList<Result> plant_details;
    public static final Integer RecordAudioRequestCode = 1;

    private ImageView micButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDashboardBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.appBarDashboard.toolbar);

        /*Find view by Id*/
        sessionManager =new SessionManager();
        DrawerLayout drawer = binding.drawerLayout;
        Toolbar toolbar =binding.appBarDashboard.toolbar;
        spn_lang= binding.appBarDashboard.spnLang;
        micButton=binding.appBarDashboard.button;




         // onClick event on mic btn
         micButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ContextCompat.checkSelfPermission(Dashboard.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED){
                    checkPermission();
                }else {


                    FragmentManager fragmentManager = getSupportFragmentManager();
                    if (fragmentManager != null && fragmentManager.findFragmentByTag("dialogVoiceRecognizer") == null && !isFinishing()) {
                        languageDialogFragment = new VoiceRecognizerDialogFragment(Dashboard.this, Dashboard.this,0);

                        languageDialogFragment.show(fragmentManager, "dialogVoiceRecognizer");
                    }

                }




            }
        });





        NavigationView navigationView = binding.navView;
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,R.id.nav_share)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_dashboard);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);


        /* addtion features Add to need this implement */
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
             @Override
             public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
                 int id = item.getItemId();
                 if(id==R.id.nav_home){
                     loadFragment(new HomeFragment());
                 }else if(id==R.id.nav_language){
                     Intent i =new Intent(Dashboard.this, ChooseLanguages.class);
                     i.putExtra("check",1);
                     startActivity(i);

                     overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

                 }
                 else if(id==R.id.nav_share) {

                     final String appPackageName = getPackageName();
                     Intent sendIntent = new Intent();
                     sendIntent.setAction(Intent.ACTION_SEND);
                     sendIntent.putExtra(Intent.EXTRA_TEXT, " Download  the HerbHeal App : https://play.google.com/store/apps/details?id=" + appPackageName + "");

                     sendIntent.setType("text/plain");
                     startActivity(sendIntent);

                 }
                 drawer.closeDrawer(GravityCompat.START);
                 return true;
             }
         });




    }
    //fragment load on Dashboard calling Method
    public void loadFragment(Fragment fragment) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment_content_dashboard, fragment);
        transaction.commit();
    }





    // This callback is invoked when the Speech Recognizer returns.
    // This is where you process the intent and extract the speech text from the intent.
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                    if (resultCode ==RESULT_OK && null!=data) {
                        List<String> results = data.getStringArrayListExtra(
                                RecognizerIntent.EXTRA_RESULTS);
                        String spokenText = results.get(0);
                       // Toast.makeText(this,"speech"+spokenText,Toast.LENGTH_SHORT).show();
                        // Do something with spokenText.
                    }

                    break;

        }


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if(check==1){
                AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                        Dashboard.this);

// Setting Dialog Title
                /*alertDialog2.setTitle("Confirm Delete...");*/

// Setting Dialog Message
                alertDialog2.setMessage(getResources().getText(R.string.Are_you_sure_you_want_Exit));

// Setting Icon to Dialog
                /*alertDialog2.setIcon(R.drawable.ic_cancel);*/

// Setting Positive "Yes" Btn
                alertDialog2.setPositiveButton(getResources().getText(R.string.Yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                finishAffinity();
                            }
                        });

// Setting Negative "NO" Btn
                alertDialog2.setNegativeButton(getResources().getText(R.string.No),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog

                                dialog.cancel();
                            }
                        });

// Showing Alert Dialog
                alertDialog2.show();

            }else {
                super.onBackPressed();
            }


        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id==R.id.nav_share) {

            final String appPackageName = getPackageName();
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, " Download  the Nursing Care App : https://play.google.com/store/apps/details?id=" + appPackageName + "");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);



        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_dashboard);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    //check runtime permission of Audio
    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.RECORD_AUDIO},RecordAudioRequestCode);
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RecordAudioRequestCode && grantResults.length > 0 ){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                if (fragmentManager != null && fragmentManager.findFragmentByTag("dialogVoiceRecognizer") == null && !isFinishing()) {
                    languageDialogFragment = new VoiceRecognizerDialogFragment(Dashboard.this, Dashboard.this,0);
                    languageDialogFragment.show(fragmentManager, "dialogVoiceRecognizer");
                }
            }


        }
    }



    @Override
    public void spokenText(String spokenText) {
         plant_details.clear();

        Intent i =new Intent(Dashboard.this, DetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("details", (ArrayList<? extends Parcelable>) plant_details);
        i.putExtras(bundle);
        i.putExtra("spokenText",spokenText);
        startActivity(i);
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

    }



}