package com.example.herbheal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.herbheal.service.SessionManager;
import com.thekhaeng.pushdownanim.PushDownAnim;

import java.util.Locale;

public class ChooseLanguages extends AppCompatActivity {
    LinearLayout ln_address_english,ln_address_hindi,ln_contiue;
    CheckBox hindi_check,english_check;
    String langues=null;
    ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_languages);
       //init view by Id
        ln_address_english= findViewById(R.id.ln_address_english);
        ln_address_hindi= findViewById(R.id.ln_address_hindi);
        ln_contiue= findViewById(R.id.ln_contiue);
        hindi_check= findViewById(R.id.hindi_check);
        english_check= findViewById(R.id.english_check);
        img_back=(ImageView)findViewById(R.id.img_back);
        Intent i =getIntent();
        int check= i.getIntExtra("check",0);

        if(check==0){
            img_back.setVisibility(View.GONE);
        }else {
            img_back.setVisibility(View.VISIBLE);
        }

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        PushDownAnim.setPushDownAnimTo( ln_address_english)
                .setOnClickListener( new View.OnClickListener(){
                    @Override
                    public void onClick( View view ){
                        ln_address_english.setBackgroundResource(R.drawable.border_blue_round);
                        ln_address_hindi.setBackgroundResource(R.drawable.border_gray);
                        hindi_check.setChecked(false);
                        english_check.setChecked(true);
                        langues="English";

                    }

                } );

        // click on check box english
        english_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (english_check.isChecked()){
                    ln_address_hindi.setBackgroundResource(R.drawable.border_gray);
                    ln_address_english.setBackgroundResource(R.drawable.border_blue_round);
                    langues="English";
                    hindi_check.setChecked(false);
                }
            }
        });

        // click on check box english
        hindi_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (hindi_check.isChecked()){
                    ln_address_hindi.setBackgroundResource(R.drawable.border_blue_round);
                    ln_address_english.setBackgroundResource(R.drawable.border_gray);
                    langues="Hindi";
                    english_check.setChecked(false);
                }
            }
        });


        PushDownAnim.setPushDownAnimTo( ln_address_hindi)
                .setOnClickListener( new View.OnClickListener(){
                    @Override
                    public void onClick( View view ){
                        ln_address_hindi.setBackgroundResource(R.drawable.border_blue_round);
                        ln_address_english.setBackgroundResource(R.drawable.border_gray);
                        hindi_check.setChecked(true);
                        english_check.setChecked(false);
                        langues="Hindi";
                        english_check.setChecked(false);
                    }

                } );

        PushDownAnim.setPushDownAnimTo( ln_contiue)
                .setOnClickListener( new View.OnClickListener(){
                    @Override
                    public void onClick( View view ){
                        if(langues !=null && langues.equalsIgnoreCase("Hindi")){

                            updateViews("hi");
                        }else if(langues !=null && langues.equalsIgnoreCase("English")){
                            updateViews("eng");

                        }else {
                            Toast.makeText(ChooseLanguages.this,"Please Select One Languages",Toast.LENGTH_SHORT).show();
                        }
                    }

                } );

    }
    private void updateViews(String languageCode) {
        String languageToLoad  = languageCode;
        Locale myLocale = new Locale(languageToLoad);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        SessionManager sessionManager =new SessionManager();
        sessionManager.setPreferences(ChooseLanguages.this, "select_languages", "true");
        sessionManager.setPreferences(ChooseLanguages.this,"languages",languageCode);

        Intent intent = new Intent(ChooseLanguages.this, Dashboard.class);
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

    }
}