package com.example.herbheal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;

import com.example.herbheal.service.SessionManager;

import java.util.Locale;

public class SplashScreen extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 5000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        final SessionManager sessionManager = new SessionManager();

            //checking condition of languages.app running which current languages .and user login first time or not
            if(sessionManager.getPreferences(SplashScreen.this, "select_languages")!=null  && sessionManager.getPreferences(SplashScreen.this, "select_languages").equalsIgnoreCase("true") ){

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(sessionManager.getPreferences(SplashScreen.this, "languages").equalsIgnoreCase("hi") ){

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    updateViews("hi");

                                }
                            },SPLASH_TIME_OUT);
                        }else {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent i =new Intent(SplashScreen.this, Dashboard.class);
                                    startActivity(i);
                                    finish();
                                    overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                                }
                            },SPLASH_TIME_OUT);
                        }



                    }
                },SPLASH_TIME_OUT);

            }else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent i =new Intent(SplashScreen.this, ChooseLanguages.class);
                        i.putExtra("check",0);
                        startActivity(i);
                        finish();
                        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

                    }
                },SPLASH_TIME_OUT);


            }







    }
    //update app languages method
    private void updateViews(String languageCode) {
        String languageToLoad  = languageCode;
        Locale myLocale = new Locale(languageToLoad);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);


        Intent intent = new Intent(SplashScreen.this, Dashboard.class);
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

    }
}