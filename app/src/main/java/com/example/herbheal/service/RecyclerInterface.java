package com.example.herbheal.service;

import com.example.herbheal.Model.ChatResponse;
import com.example.herbheal.Model.PlantDetails;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface RecyclerInterface {


    @GET
    Call<PlantDetails> getDetails(@Url String url);

    @POST("chat")
    @FormUrlEncoded
    Call<ChatResponse> getChatbot(@Field("ChatInput") String ChatInput);
}
