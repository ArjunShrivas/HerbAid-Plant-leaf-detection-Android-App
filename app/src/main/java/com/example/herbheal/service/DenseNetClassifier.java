package com.example.herbheal.service;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.fragment.app.FragmentActivity;

import com.example.herbheal.ml.DenseNet;
//import com.example.herbheal.ml.InceptionModel;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

public class DenseNetClassifier {
    Context context;
    TensorBuffer outputFeature0;
    int index=0;
    float min=0.0f;
    int mInputSize=224;
    private List<String> labellist ;
    public DenseNetClassifier(Bitmap bitmap, List<String> labellist, FragmentActivity context)throws IOException {
        this.context=context;
        this.labellist=labellist;

        bitmap= Bitmap.createScaledBitmap(bitmap,mInputSize,mInputSize,true);

        DenseNet model = DenseNet.newInstance(context);

        // Creates inputs for reference.
        TensorBuffer inputFeature0 = TensorBuffer.createFixedSize(new int[]{1, 224, 224, 3}, DataType.FLOAT32);

        TensorImage tensorImage =new TensorImage(DataType.FLOAT32);
        tensorImage.load(bitmap);
        ByteBuffer byteBuffer =tensorImage.getBuffer();

        inputFeature0.loadBuffer(byteBuffer);


        // Runs model inference and gets result.
        DenseNet.Outputs outputs = model.process(inputFeature0);
        outputFeature0 = outputs.getOutputFeature0AsTensorBuffer();

        // Releases model resources if no longer used.
        model.close();


    }
    // find maxmimum probablity
    public int getmax(){
        for (int i=0;i<labellist.size();i++){
            if(outputFeature0.getFloatArray()[i]>min){
                index=i;
                min=outputFeature0.getFloatArray()[i];
            }
        }

        return  index;
    }
    public float getAucc(){
        return outputFeature0.getFloatArray()[index];

    }

}
