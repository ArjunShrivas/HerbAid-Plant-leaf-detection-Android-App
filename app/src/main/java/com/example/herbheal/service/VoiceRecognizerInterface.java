package com.example.herbheal.service;

import com.example.herbheal.Model.Result;

import java.util.ArrayList;

public interface VoiceRecognizerInterface {
    void spokenText(String spokenText);
}
