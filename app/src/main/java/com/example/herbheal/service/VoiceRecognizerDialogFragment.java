package com.example.herbheal.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.herbheal.Model.ChatResponse;
import com.example.herbheal.R;
import com.example.herbheal.Model.PlantDetails;
import com.example.herbheal.Model.Result;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;

public class VoiceRecognizerDialogFragment extends DialogFragment implements RecognitionListener {

    ImageView micImage;
    TextView stateTV;
    TextView displayTV;


    private SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;
    private Context context;
    VoiceRecognizerInterface signal;
    int errors;

    @SuppressLint("ValidFragment")
    public VoiceRecognizerDialogFragment(Context context, VoiceRecognizerInterface signal,int error) {
        this.context = context;
        this.signal = signal;
        this.errors=error;
    }

    @Override
    public void onResume() {
        super.onResume();

        int width = getResources().getDimensionPixelSize(R.dimen.popup_width);
        int height = getResources().getDimensionPixelSize(R.dimen.popup_height);
        getDialog().getWindow().setLayout(width, height);
        if(errors==1){
            changeUIStateToRetry();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.voice_recognizer_custom_layout ,container, false );

        //Mic tap to listen again
        micImage = view.findViewById(R.id.micImageView);
        //Displays Listening.. when recognizer is listening
        stateTV = view.findViewById(R.id.stateTV);
        //Displays message if error
        displayTV = view.findViewById(R.id.displayTV);


        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Say Somthing Here");

        mSpeechRecognizer.setRecognitionListener(this);
        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
        micImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startListening();
            }
        });
        return view;

    }

    public void startListening(){
        displayTV.setText("Tell us what you need");
        stateTV.setText("Listening...");
        mSpeechRecognizer.setRecognitionListener(this);
        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
        changeUIStateToListening();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSpeechRecognizer != null) {
            mSpeechRecognizer.destroy();
        }
    }

    @Override
    public void onBeginningOfSpeech()
    {
    }

    @Override
    public void onBufferReceived(byte[] buffer)
    {

    }

    @Override
    public void onEndOfSpeech()
    {

    }

    @Override
    public void onError(int error)
    {
        if(error == 7){
            changeUIStateToRetry();
        }
    }

    @Override
    public void onEvent(int eventType, Bundle params)
    {

    }

    @Override
    public void onPartialResults(Bundle partialResults) {
    }

    @Override
    public void onReadyForSpeech(Bundle params)
    {

    }

    @Override
    public void onResults(Bundle results)
    {
        ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

        if(matches == null){
            return;
        }
        int i =0;
        String first ="";
        for(String s : matches){
            if(i==0){
                first = s;
            }
            i++;
        }
        // sending text to MainActivity using Interface


        signal.spokenText(first);

        this.dismiss();
        //stateTV.setText(first);

    }

    @Override
    public void onRmsChanged(float rmsdB)
    {
    }

    public void changeUIStateToListening(){
        displayTV.setText("Tell us what you need");
        stateTV.setText("Listening...");
    }

    public void changeUIStateToRetry(){
        displayTV.setText("Didn't catch that. Try\nSpeaking again");
        stateTV.setText("Tap on mic to try again");
    }




}